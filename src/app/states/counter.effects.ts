import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { ActionTypes } from './counter.actions';
import { tap, switchMap } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

@Injectable()
export class CounterEffects {

  constructor(
    private actions$: Actions
  ) {}

  @Effect()
  LogActionAtIncrement = this.actions$.pipe(
    ofType(ActionTypes.Increment),
    tap(data => {

      console.log('Someone clicked the increment button');

    }),
    switchMap(data => {
      return EMPTY;
    })
  );
}
